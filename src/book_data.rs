use js_sys::JsString;
use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use super::book_components::{Copyright, Publisher};

#[derive(Clone, Debug, Deserialize, Serialize)]
#[wasm_bindgen]
pub struct BookData {
    #[wasm_bindgen(skip)]
    pub title: String,
    #[wasm_bindgen(skip)]
    pub subtitles: Vec<String>,
    #[wasm_bindgen(skip)]
    pub edition: String,
    #[wasm_bindgen(skip)]
    pub printer: String,
    #[wasm_bindgen(skip)]
    pub prefaces: Vec<String>,
    #[wasm_bindgen(skip)]
    pub acknowledgements: Vec<String>,
    #[wasm_bindgen(skip)]
    pub copyright: Copyright,
    #[wasm_bindgen(skip)]
    pub publisher: Publisher,
}

#[wasm_bindgen]
impl BookData {
    #[wasm_bindgen(getter)]
    pub fn title(&self) -> String {
        self.title.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn subtitles(&self) -> Vec<JsValue> {
        self.subtitles.iter().map(|value| JsValue::from_str(value)).collect::<Vec<JsValue>>()
    }

    #[wasm_bindgen(getter)]
    pub fn edition(&self) -> String {
        self.edition.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn printer(&self) -> String {
        self.printer.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn prefaces(&self) -> Vec<JsString> {
        self.prefaces
            .iter()
            .map(|name| JsString::from(name.as_str()))
            .collect::<Vec<JsString>>()
    }

    #[wasm_bindgen(getter)]
    pub fn acknowledgements(&self) -> Vec<JsString> {
        self.acknowledgements
            .iter()
            .map(|name| JsString::from(name.as_str()))
            .collect::<Vec<JsString>>()
    }

    #[wasm_bindgen(getter)]
    pub fn copyright(&self) -> Copyright {
        self.copyright.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn publisher(&self) -> Publisher {
        self.publisher.clone()
    }
}
