use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen]
#[derive(Clone, Copy, Debug)]
pub enum BookStructureErrorType {
    InvalidUtf8,
}