use snafu::Snafu;
use wasm_bindgen::prelude::wasm_bindgen;
use zip::result::ZipError;
use super::SaveToFileErrorType;

#[wasm_bindgen]
#[derive(Debug, Snafu)]
pub struct SaveToFileError {
    #[wasm_bindgen(skip)]
    pub message: String,
    pub error_type: SaveToFileErrorType,
}

impl SaveToFileError {
    pub fn new(message: String, error_type: SaveToFileErrorType) -> Self {
        Self {
            message,
            error_type,
        }
    }
}

#[wasm_bindgen]
impl SaveToFileError {
    #[wasm_bindgen(getter)]
    pub fn message(&self) -> String {
        self.message.clone()
    }
}

impl From<ZipError> for SaveToFileError {
    fn from(zip_error: ZipError) -> Self {
        Self::new(zip_error.to_string(), SaveToFileErrorType::ZipError)
    }
}

impl From<std::io::Error> for SaveToFileError {
    fn from(zip_error: std::io::Error) -> Self {
        Self::new(zip_error.to_string(), SaveToFileErrorType::IOError)
    }
}

impl From<serde_json::Error> for SaveToFileError {
    fn from(zip_error: serde_json::Error) -> Self {
        Self::new(zip_error.to_string(), SaveToFileErrorType::SerializationError)
    }
}
