use wasm_bindgen::prelude::wasm_bindgen;
use super::BookStructureErrorType;

#[wasm_bindgen]
#[derive(Debug)]
pub struct BookStructureError {
    #[wasm_bindgen(skip)]
    pub message: String,
    pub error_type: BookStructureErrorType,
}

impl BookStructureError {
    pub fn new(message: String, error_type: BookStructureErrorType) -> Self {
        Self {
            message,
            error_type,
        }
    }
}