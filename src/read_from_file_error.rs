use snafu::Snafu;
use wasm_bindgen::prelude::wasm_bindgen;
use zip::result::ZipError;
use crate::ReadFromFileErrorType;

#[wasm_bindgen]
#[derive(Debug, Snafu)]
pub struct ReadFromFileError {
    #[wasm_bindgen(skip)]
    pub message: String,
    pub error_type: ReadFromFileErrorType,
}

impl ReadFromFileError {
    pub fn new(message: String, error_type: ReadFromFileErrorType) -> Self {
        Self {
            message,
            error_type,
        }
    }
}

#[wasm_bindgen]
impl ReadFromFileError {
    #[wasm_bindgen(getter)]
    pub fn message(&self) -> String {
        self.message.clone()
    }
}

impl From<ZipError> for ReadFromFileError {
    fn from(zip_error: ZipError) -> Self {
        Self::new(zip_error.to_string(), ReadFromFileErrorType::ZipError)
    }
}

impl From<std::io::Error> for ReadFromFileError {
    fn from(zip_error: std::io::Error) -> Self {
        Self::new(zip_error.to_string(), ReadFromFileErrorType::IOError)
    }
}

impl From<serde_json::Error> for ReadFromFileError {
    fn from(zip_error: serde_json::Error) -> Self {
        Self::new(zip_error.to_string(), ReadFromFileErrorType::DeserializationError)
    }
}
