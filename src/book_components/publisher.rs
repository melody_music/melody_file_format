use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::wasm_bindgen;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[wasm_bindgen]
pub struct Publisher {
    #[wasm_bindgen(skip)]
    pub name: String,
    #[wasm_bindgen(skip)]
    pub location: String,
}

#[wasm_bindgen]
impl Publisher {
    #[wasm_bindgen(getter)]
    pub fn name(&self) -> String {
        self.name.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn location(&self) -> String {
        self.location.clone()
    }
}
