use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::wasm_bindgen;

#[derive(Clone, Debug, Deserialize, Serialize)]
#[wasm_bindgen]
pub struct Copyright {
    pub year: u32,
    pub month: u8,
}
