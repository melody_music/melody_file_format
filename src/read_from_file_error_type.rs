use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen]
#[derive(Clone, Copy, Debug)]
pub enum ReadFromFileErrorType {
    ZipError,
    DeserializationError,
    IOError,
}