use wasm_bindgen::prelude::wasm_bindgen;
use crate::BookFileDataErrorType;

#[wasm_bindgen]
pub struct BookFileDataError {
    #[wasm_bindgen(skip)]
    pub message: String,
    #[wasm_bindgen(skip)]
    pub error_type: BookFileDataErrorType,
}

impl BookFileDataError {
    pub fn new(message: String, error_type: BookFileDataErrorType) -> Self {
        Self {
            message,
            error_type,
        }
    }
}

#[wasm_bindgen]
impl BookFileDataError {
    #[wasm_bindgen(getter)]
    pub fn message(&self) -> String {
        self.message.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn error_type(&self) -> BookFileDataErrorType {
        self.error_type
    }
}

impl From<serde_json::Error> for BookFileDataError {
    fn from(zip_error: serde_json::Error) -> Self {
        Self::new(zip_error.to_string(), BookFileDataErrorType::DeserializationError)
    }
}
