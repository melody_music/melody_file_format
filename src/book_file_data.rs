use std::io::Write;
use chrono::{DateTime, Utc};
use indicatif::ProgressBar;

use js_sys::Date;
use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;
use zip::write::FileOptions;
use zip::ZipWriter;

use crate::book_data::BookData;
use crate::{BookFileDataError, BookStructure, MusicFile, SaveToFileError};

#[derive(Clone, Debug, Deserialize, Serialize,)]
#[wasm_bindgen]
pub struct BookFileData {
    #[wasm_bindgen(skip)]
    pub book_data: BookData,
    #[wasm_bindgen(skip)]
    pub book_structure: BookStructure,
    #[wasm_bindgen(skip)]
    pub build_time: DateTime<Utc>,
}

impl BookFileData {
    pub fn new(book_data: BookData, book_structure: BookStructure) -> Self {
        Self { book_data, book_structure, build_time: Utc::now() }
    }

    pub fn to_zip_bytes(
        &self,
        score_files: &Vec<MusicFile>,
        optional_progress_bar: Option<ProgressBar>) -> Result<Vec<u8>, SaveToFileError> {
        let mut output = Vec::new();
        let cursor = std::io::Cursor::new(&mut output);
        let mut zip_writer = ZipWriter::new(cursor);
        zip_writer.add_directory("musicxml", FileOptions::default())?;
        for file in score_files {
            zip_writer.start_file(format!("musicxml/{}", file.name), FileOptions::default())?;
            zip_writer.write_all(file.contents.as_bytes())?;
            if let Some(progress_bar) = &optional_progress_bar {
                progress_bar.inc(1);
            }
        }

        if let Some(progress_bar) = &optional_progress_bar {
            progress_bar.finish_and_clear();
        }

        let mut book_config_file_names = score_files
            .iter()
            .map(|file| file.name.clone())
            .collect::<Vec<String>>();
        book_config_file_names.sort();
        let json_string = serde_json::to_string(self)?;
        zip_writer.start_file("book.json", FileOptions::default())?;
        zip_writer.write_all(json_string.as_bytes())?;
        return Ok((*zip_writer.finish()?.into_inner()).clone());
    }
}

#[wasm_bindgen]
impl BookFileData {
    #[wasm_bindgen(constructor)]
    pub fn js_constructor_from_string(input: String) -> Result<BookFileData, BookFileDataError> {
        Ok(serde_json::from_str(&input)?)
    }

    #[wasm_bindgen(getter)]
    pub fn book_data(&self) -> BookData {
        self.book_data.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn book_structure(&self) -> BookStructure {
        self.book_structure.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn build_time(&self) -> Date {
        Date::new(&JsValue::from(&self.build_time.to_rfc3339()))
    }

    pub fn serialize_to_string(&self) -> Result<String, BookFileDataError> {
        Ok(serde_json::to_string(&self)?)
    }
}
