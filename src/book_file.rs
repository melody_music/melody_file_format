use std::collections::HashMap;
use std::io::{Cursor, Read, Write};

use indicatif::ProgressBar;
use js_sys::Uint8Array;
use wasm_bindgen::prelude::*;
use zip::{ZipArchive, ZipWriter};
use zip::write::FileOptions;

use crate::{BookFileData};

use crate::save_to_file_error::SaveToFileError;
use crate::utils::set_panic_hook;

#[derive(Debug)]
#[wasm_bindgen]
pub struct BookFile {
    #[wasm_bindgen(skip)]
    pub data: BookFileData,
    #[wasm_bindgen(skip)]
    pub files: HashMap<String, String>
}

impl BookFile {
    pub fn new(data: BookFileData, files: HashMap<String, String>) -> Self {
        Self { data, files }
    }

    pub fn to_zip_bytes(
        &self,
        optional_progress_bar: Option<&ProgressBar>) -> Result<Vec<u8>, SaveToFileError> {
        let mut output = Vec::new();
        let cursor = Cursor::new(&mut output);
        let mut zip_writer = ZipWriter::new(cursor);
        zip_writer.add_directory("musicxml", FileOptions::default())?;
        for file in &self.files {
            zip_writer.start_file(format!("musicxml/{}", file.0), FileOptions::default())?;
            zip_writer.write_all(file.1.as_bytes())?;
            if let Some(progress_bar) = &optional_progress_bar {
                progress_bar.inc(1);
            }
        }

        if let Some(progress_bar) = &optional_progress_bar {
            progress_bar.finish_and_clear();
        }

        let json_string = serde_json::to_string(&self.data)?;
        zip_writer.start_file("book.json", FileOptions::default())?;
        zip_writer.write_all(json_string.as_bytes())?;
        return Ok((*zip_writer.finish()?.into_inner()).clone());
    }

    pub fn from_zip_bytes(zip_bytes: Vec<u8>) -> Self {
        let cursor = Cursor::new(zip_bytes);
        let mut reader =  ZipArchive::new(cursor).unwrap();

        let mut data_file_content = String::new();
        reader.by_name("book.json").unwrap().read_to_string(&mut data_file_content).unwrap();
        let data: BookFileData = serde_json::from_str(&data_file_content).unwrap();

        let mut music_files = HashMap::new();

        for filename in &data.book_structure.file_names {
            let mut file_content = String::new();
            reader.by_name(&format!("musicxml/{}", filename)).unwrap().read_to_string(&mut file_content).unwrap();
            music_files.insert(filename.clone(), file_content);
        }

        Self::new(data, music_files)
    }
}

#[wasm_bindgen]
impl BookFile {
    #[wasm_bindgen(constructor)]
    pub fn js_constructor_from_zip_bytes(zip_bytes: Uint8Array) -> BookFile {
        set_panic_hook();
        let mut native_bytes = vec![0u8; zip_bytes.length() as usize];
        zip_bytes.copy_to(&mut native_bytes);

        assert_eq!(zip_bytes.length() as usize, native_bytes.len());
        for i in 0..native_bytes.len() {
            assert_eq!(native_bytes[i], zip_bytes.at(i as i32).unwrap());
        }

        Self::from_zip_bytes(native_bytes)
    }

    pub fn save_to_file(&self) -> Result<Uint8Array, js_sys::Error> {
        match self.to_zip_bytes(None) {
            Ok(vec) => Ok(Uint8Array::from(&vec[..])),
            Err(error) => Err(js_sys::Error::new(&error.message)),
        }
    }

    #[wasm_bindgen(getter)]
    pub fn data(&self) -> BookFileData {
        self.data.clone()
    }

    pub fn get_file_content(&self, file_name: String) -> String {
        self.files[&file_name].clone()
    }
}
