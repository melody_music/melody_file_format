use js_sys::JsString;
use serde_derive::{Deserialize, Serialize};
use wasm_bindgen::prelude::wasm_bindgen;
use crate::{BookStructureError, BookStructureErrorType};

#[derive(Clone, Debug, Deserialize, Serialize)]
#[wasm_bindgen]
pub struct BookStructure {
    #[wasm_bindgen(skip)]
    pub file_names: Vec<String>,
}

impl BookStructure {
    pub fn new(file_names: Vec<String>) -> Self {
        Self {
            file_names,
        }
    }
}

#[wasm_bindgen]
impl BookStructure {
    #[wasm_bindgen(constructor)]
    pub fn js_constructor(js_file_names: Vec<JsString>) -> Result<BookStructure, BookStructureError> {
        let converted_names = js_file_names
            .iter()
            .map(|name| name.as_string()).collect::<Vec<Option<String>>>();
        let mut file_names = Vec::new();
        for converted_name in converted_names {
             match converted_name {
                 Some(name) => file_names.push(name),
                 None => return Err(BookStructureError::new(
                     "Could not convert JsString to Rust String".to_string(),
                     BookStructureErrorType::InvalidUtf8)),
            }
        }
        Ok(Self::new(file_names))
    }

    #[wasm_bindgen(getter)]
    pub fn file_names(&self) -> Vec<JsString> {
        self.file_names
            .iter()
            .map(|name| JsString::from(name.as_str()))
            .collect::<Vec<JsString>>()
    }
}
