extern crate core;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub mod book_components;

pub mod book_data;
pub use book_data::BookData;

pub mod book_file;
pub use book_file::BookFile;

pub mod book_file_data;
pub use book_file_data::BookFileData;

pub mod book_file_data_error;
pub use book_file_data_error::BookFileDataError;

pub mod book_file_data_error_type;
pub use book_file_data_error_type::BookFileDataErrorType;

pub mod book_structure;
pub use book_structure::BookStructure;

pub mod book_structure_error;
pub use book_structure_error::BookStructureError;

pub mod book_structure_error_type;
pub use book_structure_error_type::BookStructureErrorType;

pub mod music_file;
pub use music_file::MusicFile;

pub mod read_from_file_error;
pub use read_from_file_error::ReadFromFileError;

pub mod read_from_file_error_type;
pub use read_from_file_error_type::ReadFromFileErrorType;

pub mod save_to_file_error;
pub use save_to_file_error::SaveToFileError;

pub mod save_to_file_error_type;
pub use save_to_file_error_type::SaveToFileErrorType;

mod utils;